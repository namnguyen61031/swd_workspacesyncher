/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.TrackerController;
import dal.FileInformationDAO;
import java.io.File;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author Nam Nguyen
 */
public class FileInformation extends File{
    
    private int id;
    private String absolutePath;
    private boolean directory;
    private String remoteParentId;
    private String remoteFileId;
    private String rootPath;
    private int status;
    private Date lastModifiedTime;
    private int accountId;
    
    public static final int STATUS_DELIVERED = 0;
    public static final int STATUS_CHANGED = 1;
    public static final int STATUS_REMOVED = 2;
    
    public final static HashMap<Integer, String> STATUS_MAPPER = new HashMap<>();
    static{
        STATUS_MAPPER.put(STATUS_CHANGED, "changed");
        STATUS_MAPPER.put(STATUS_DELIVERED, "delivered");
        STATUS_MAPPER.put(STATUS_REMOVED, "removed");
    }
    

    public FileInformation(int id, String absolutePath, boolean directory, String remoteParentId, String remoteFileId, String rootPath, int status, Date lastModifiedTime, int accountId) {
        super(absolutePath);
        this.id = id;
        this.absolutePath = absolutePath;
        this.directory = directory;
        this.remoteFileId = remoteFileId;
        this.remoteParentId = remoteParentId;
        this.rootPath = rootPath;
        this.status = status;
        this.lastModifiedTime = lastModifiedTime;
        this.accountId = accountId;
    }
    
    public FileInformation(String absolutePath) {
        super(absolutePath);
        this.absolutePath = absolutePath;
        this.directory = super.isDirectory();
        this.lastModifiedTime = new Date(super.lastModified());
        this.accountId = TrackerController.account.getId();
    }

    public FileInformation(String absolutePath, int status, int accountId) {
        super(absolutePath);
        this.absolutePath = absolutePath;
        this.directory = super.isDirectory();
        this.lastModifiedTime = new Date(super.lastModified());
        this.status = status;
        this.accountId = accountId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }


    public String getRemoteParentId() {
        return remoteParentId;
    }

    public void setRemoteParentId(String remoteParentId) {
        this.remoteParentId = remoteParentId;
    }

    public String getRemoteFileId() {
        return remoteFileId;
    }

    public void setRemoteFileId(String remoteFileId) {
        this.remoteFileId = remoteFileId;
    }

    @Override
    public boolean isDirectory() {
        return directory;
    }

    public void setDirectory(boolean directory) {
        this.directory = directory;
    }

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }
    
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }

    public Date getLastModifiedTime() {
        return lastModifiedTime;
    }

    public void setLastModifiedTime(Date lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    @Override
    public FileInformation getParentFile() {
        return new FileInformationDAO().getFileInformationByPath(this.getParent(), this.accountId);
    }
    
    @Override
    public String toString() {
        return super.getName() + "   ("+STATUS_MAPPER.get(status)+")"; //To change body of generated methods, choose Tools | Templates.
    }
}
