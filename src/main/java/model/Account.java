/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nam Nguyen
 */
public class Account {
    private int id;
    private String gmail;
    private boolean autoSynched;

    public Account() {
    }

    public Account(String gmail, boolean autoSynched) {
        this.gmail = gmail;
        this.autoSynched = autoSynched;
    }

    public Account(int id, String gmail, boolean autoSynched) {
        this.id = id;
        this.gmail = gmail;
        this.autoSynched = autoSynched;
    }
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGmail() {
        return gmail;
    }

    public void setGmail(String gmail) {
        this.gmail = gmail;
    }

    public boolean isAutoSynched() {
        return autoSynched;
    }

    public void setAutoSynched(boolean autoSynched) {
        this.autoSynched = autoSynched;
    }
    
    
}
