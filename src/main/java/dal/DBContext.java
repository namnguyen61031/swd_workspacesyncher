/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Atheros
 */
public class DBContext {
    private final String dbName = "SWD_workspace_synch";
    private final String username = "sa";
    private final String password = "123456";
    private final String hostname = "localhost";
    private final String port = "1433";
    
    public Connection getConnection() throws Exception {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        return DriverManager.getConnection("jdbc:sqlserver://" + hostname + ":" + port + ";databaseName=" + dbName, username, password);
    }
}
