/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import controller.TrackerController;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.FileInformation;
import utils.Close;

/**
 *
 * @author Atheros
 */
public class FileInformationDAO {

    private final DBContext db;
    private final Close close;

    public FileInformationDAO() {
        this.db = new DBContext();
        this.close = new Close();
    }

    public boolean createFileInformation(FileInformation fileInformation) {
        String sqlCommand = "INSERT INTO [FileInformation](absolutePath, "
                + "isDirectory,remoteParentId, remoteFileId, rootPath, status, lastModifiedTime, "
                + "accountId) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";

        Connection conn = null;
        PreparedStatement ps = null;
        boolean isSucess = true;

        try {
            conn = db.getConnection();
            ps = conn.prepareStatement(sqlCommand);
            ps.setString(1, fileInformation.getAbsolutePath());
            ps.setBoolean(2, fileInformation.isDirectory());

            ps.setString(3, fileInformation.getRemoteParentId());
            ps.setString(4, fileInformation.getRemoteFileId());
            ps.setString(5, fileInformation.getRootPath());
            ps.setInt(6, fileInformation.getStatus());
            System.out.println(fileInformation.getLastModifiedTime());
            ps.setTimestamp(7, new Timestamp(fileInformation.getLastModifiedTime().getTime()));

            ps.setInt(8, fileInformation.getAccountId());
            ps.executeUpdate();
        } catch (Exception ex) {
            isSucess = false;
            Logger.getLogger(FileInformationDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            close.closePreparedStatement(ps);
            close.closeConnection(conn);
        }

        return isSucess;
    }

    public boolean deleteFileInformation(int fileID) {
        String sqlCommand = "DELETE FROM [FileInformation] WHERE id = ?";
        Connection conn = null;
        PreparedStatement ps = null;
        boolean isSucess = true;

        try {
            conn = db.getConnection();
            ps = conn.prepareStatement(sqlCommand);
            ps.setInt(1, fileID);
            ps.executeUpdate();
        } catch (Exception ex) {
            isSucess = false;
            Logger.getLogger(FileInformationDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            close.closePreparedStatement(ps);
            close.closeConnection(conn);
        }

        return isSucess;
    }

    public ArrayList<FileInformation> getAllTrackedFolders(Account account) {
        String sqlCommand = "SELECT * FROM [FileInformation] WHERE accountId = ?"
                + " and rootPath = absolutePath";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<FileInformation> trackedFolders = new ArrayList<>();

        try {
            conn = db.getConnection();
            ps = conn.prepareStatement(sqlCommand);
            ps.setInt(1, account.getId());
            rs = ps.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String absolutePath = rs.getString("absolutePath");
                boolean directory = rs.getBoolean("isDirectory");
                String remoteParentId = rs.getString("remoteParentId");
                String remoteFileId = rs.getString("remoteFileId");
                String rootPath = rs.getString("rootPath");
                int status = rs.getInt("status");
                Date lastModifiedTime = rs.getDate("lastModifiedTime");
                int accountId = rs.getInt("accountId");

                trackedFolders.add(
                        new FileInformation(id, absolutePath, directory, remoteParentId,
                                remoteFileId, rootPath, status,
                                lastModifiedTime, accountId));
            }

        } catch (Exception ex) {
            Logger.getLogger(FileInformationDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            close.closeResultSet(rs);
            close.closePreparedStatement(ps);
            close.closeConnection(conn);
        }
        return trackedFolders;
    }

    public boolean updateFileInformationStatus(int fileID, int status) {
        String sqlCommand = "UPDATE [FileInformation] SET status = ? WHERE id = ?";
        Connection conn = null;
        PreparedStatement ps = null;
        boolean isSucess = true;

        try {
            conn = db.getConnection();
            ps = conn.prepareStatement(sqlCommand);
            ps.setInt(1, status);
            ps.setInt(2, fileID);
            ps.executeUpdate();
        } catch (Exception ex) {
            isSucess = false;
            Logger.getLogger(FileInformationDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            close.closePreparedStatement(ps);
            close.closeConnection(conn);
        }

        return isSucess;
    }

    public boolean updateLastModificationTime(int fileID, Date datetime) {
        String sqlCommand = "UPDATE [FileInformation] SET lastModifiedTime = ? WHERE id = ?";
        Connection conn = null;
        PreparedStatement ps = null;
        boolean isSucess = true;

        try {
            conn = db.getConnection();
            ps = conn.prepareStatement(sqlCommand);
            ps.setTimestamp(1, new Timestamp(datetime.getTime()));
            ps.setInt(2, fileID);
            ps.executeUpdate();
        } catch (Exception ex) {
            isSucess = false;
            Logger.getLogger(FileInformationDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            close.closePreparedStatement(ps);
            close.closeConnection(conn);
        }

        return isSucess;
    }

    public boolean updateFileInformationStatusAndLastModificationTime(int fileID, int status, Date datetime){
        String sqlCommand = "UPDATE [FileInformation] SET status = ?, lastModifiedTime = ? WHERE id = ?";
        Connection conn = null;
        PreparedStatement ps = null;
        boolean isSucess = true;
        
        try {
            conn = db.getConnection();
            ps = conn.prepareStatement(sqlCommand);
            ps.setInt(1, status);
            ps.setTimestamp(2, new Timestamp(datetime.getTime()));
            ps.setInt(3, fileID);
            ps.executeUpdate();
        } catch (Exception ex) {
            isSucess = false;
            Logger.getLogger(FileInformationDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            close.closePreparedStatement(ps);
            close.closeConnection(conn);
        }

        return isSucess;
    }
    
    public ArrayList<FileInformation> getAllSubFileInformationsLevelOne(String parentAsolutePath) {
        String sqlCommand = "SELECT * FROM [FileInformation] WHERE absolutePath like ? and accountId=?";
        Connection conn = null;
        PreparedStatement ps = null;
        boolean isSucess = true;

        ArrayList<FileInformation> listSubItems = new ArrayList<>();

        try {
            conn = db.getConnection();
            ps = conn.prepareStatement(sqlCommand);
            ps.setString(1, parentAsolutePath+ "\\%[\\]%");
            System.out.println("\n\nParent absolute: "+parentAsolutePath);
            ps.setInt(2, TrackerController.account.getId());

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String absolutePath = rs.getString("absolutePath");
                boolean directory = rs.getBoolean("isDirectory");
                String remoteFileId = rs.getString("remoteFileId");
                String rootPath = rs.getString("rootPath");
                int status = rs.getInt("status");
                Date lastModifiedTime = rs.getDate("lastModifiedTime");
                int accountId = rs.getInt("accountId");
                System.out.println("Cild path: "+absolutePath);
                listSubItems.add(
                        new FileInformation(id, absolutePath, directory, null,
                                remoteFileId, rootPath, status,
                                lastModifiedTime, accountId));
            }
        } catch (Exception ex) {
            Logger.getLogger(FileInformationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listSubItems;
    }

    
    public ArrayList<FileInformation> getAllSubFileInformations(String parentAsolutePath) {
        String sqlCommand = "SELECT * FROM [FileInformation] WHERE absolutePath like ? and accountId=?";
        Connection conn = null;
        PreparedStatement ps = null;
        boolean isSucess = true;

        ArrayList<FileInformation> listSubItems = new ArrayList<>();

        try {
            conn = db.getConnection();
            ps = conn.prepareStatement(sqlCommand);
            ps.setString(1, parentAsolutePath+ "\\%");
            ps.setInt(2, TrackerController.account.getId());

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String absolutePath = rs.getString("absolutePath");
                boolean directory = rs.getBoolean("isDirectory");
                String remoteFileId = rs.getString("remoteFileId");
                String rootPath = rs.getString("rootPath");
                int status = rs.getInt("status");
                Date lastModifiedTime = rs.getDate("lastModifiedTime");
                int accountId = rs.getInt("accountId");

                listSubItems.add(
                        new FileInformation(id, absolutePath, directory, null,
                                remoteFileId, rootPath, status,
                                lastModifiedTime, accountId));
            }
        } catch (Exception ex) {
            Logger.getLogger(FileInformationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listSubItems;
    }

    public FileInformation getFileInformationByPath(String path, int accountID) {
        String sqlCommand = "SELECT * FROM [FileInformation] WHERE accountId = ?"
                + " and absolutePath = ?";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        FileInformation fileInformation = null;

        try {
            conn = db.getConnection();
            ps = conn.prepareStatement(sqlCommand);
            ps.setInt(1, accountID);
            ps.setString(2, path);
            rs = ps.executeQuery();

            if (rs.next()) {
                int id = rs.getInt("id");
                String absolutePath = rs.getString("absolutePath");
                boolean directory = rs.getBoolean("isDirectory");
                String remoteParentId = rs.getString("remoteParentId");
                String remoteFileId = rs.getString("remoteFileId");
                String rootPath = rs.getString("rootPath");
                int status = rs.getInt("status");
                Date lastModifiedTime = new Date(rs.getTimestamp("lastModifiedTime").getTime());
                int accountId = rs.getInt("accountId");
//                System.out.println("DAO remote parent id: " + remoteParentId);
                fileInformation = new FileInformation(id, absolutePath, directory, remoteParentId,
                        remoteFileId, rootPath, status,
                        lastModifiedTime, accountId);
            }

        } catch (Exception ex) {
            Logger.getLogger(FileInformationDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            close.closeResultSet(rs);
            close.closePreparedStatement(ps);
            close.closeConnection(conn);
        }
        return fileInformation;
    }

    
    public boolean updateAfterSynch(FileInformation fileInformation) {
        String sqlCommand = "UPDATE [FileInformation] SET remoteFileId=?, remoteParentId=?, status=? WHERE id = ?";
        Connection conn = null;
        PreparedStatement ps = null;
        boolean isSucess = true;

        try {
            conn = db.getConnection();
            ps = conn.prepareStatement(sqlCommand);
            ps.setString(1, fileInformation.getRemoteFileId());
            ps.setString(2, fileInformation.getRemoteParentId());
            ps.setInt(3, fileInformation.getStatus());
//            ps.setTimestamp(4, new Timestamp(fileInformation.getLastModifiedTime().getTime()));
            ps.setInt(4, fileInformation.getId());
            ps.executeUpdate();
        } catch (Exception ex) {
            isSucess = false;
            Logger
                    .getLogger(FileInformationDAO.class
                            .getName()).log(Level.SEVERE, null, ex);
        } finally {
            close.closePreparedStatement(ps);
            close.closeConnection(conn);
        }

        return isSucess;
    }

    public boolean deleteFolderAndSubitems(String path, int accountId) {
        String sqlCommand = "DELETE FROM [FileInformation] WHERE absolutePath like ? and accountId=?";
        Connection conn = null;
        PreparedStatement ps = null;
        boolean isSucess = true;

        try {
            conn = db.getConnection();
            ps = conn.prepareStatement(sqlCommand);
            ps.setString(1, path + "%");
            ps.setInt(2, accountId);
            ps.executeUpdate();
        } catch (Exception ex) {
            isSucess = false;
            Logger
                    .getLogger(FileInformationDAO.class
                            .getName()).log(Level.SEVERE, null, ex);
        } finally {
            close.closePreparedStatement(ps);
            close.closeConnection(conn);
        }

        return isSucess;
    }
    
    public boolean updateDeleletionStatus(String path, int accountId) {
        String sqlCommand = "UPDATE [FileInformation] SET status = ? WHERE absolutePath like ? and accountId=?";
        Connection conn = null;
        PreparedStatement ps = null;
        boolean isSucess = true;

        try {
            conn = db.getConnection();
            ps = conn.prepareStatement(sqlCommand);
            ps.setInt(1, FileInformation.STATUS_REMOVED);
            ps.setString(2, path + "%");
            ps.setInt(3, accountId);
            ps.executeUpdate();
        } catch (Exception ex) {
            isSucess = false;
            Logger
                    .getLogger(FileInformationDAO.class
                            .getName()).log(Level.SEVERE, null, ex);
        } finally {
            close.closePreparedStatement(ps);
            close.closeConnection(conn);
        }

        return isSucess;
    }
    
    public int getAllUndeliveredSubitemsCount(FileInformation fileInformation){
        String sqlCommand = "select count(*) cnt from FileInformation where status != ? and absolutePath like ? and accountId = ?";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = db.getConnection();
            ps = conn.prepareStatement(sqlCommand);
            ps.setInt(1, FileInformation.STATUS_DELIVERED);
            ps.setString(2, fileInformation.getAbsolutePath() + "\\%");
            ps.setInt(3, TrackerController.account.getId());
            rs = ps.executeQuery();

            if (rs.next()) {
               return rs.getInt("cnt");
            }

        } catch (Exception ex) {
            Logger.getLogger(FileInformationDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            close.closeResultSet(rs);
            close.closePreparedStatement(ps);
            close.closeConnection(conn);
        }
        return -1;
    }
    
    public boolean isAllDeliver(String parentPath, int accountId){
        String sqlCommand = "SELECT (SELECT COUNT(*) FROM FileInformation WHERE "
                + "absolutePath LIKE ? and accountId = ?) - (SELECT COUNT(*) "
                + "FROM FileInformation WHERE absolutePath LIKE ? and "
                + "status = 0 and accountId = ?) as different";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int numberOfUndeliverd = 0;
 
        try {
            conn = db.getConnection();
            ps = conn.prepareStatement(sqlCommand);
            ps.setString(1, parentPath + "\\%");
            ps.setInt(2, accountId);
            ps.setString(3, parentPath + "\\%");
            ps.setInt(4, accountId);
            rs = ps.executeQuery();
 
            if (rs.next()) {
               numberOfUndeliverd = rs.getInt("different");
            }
 
        } catch (Exception ex) {
            Logger.getLogger(FileInformationDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            close.closeResultSet(rs);
            close.closePreparedStatement(ps);
            close.closeConnection(conn);
        }
        return numberOfUndeliverd == 0;
    }

}
