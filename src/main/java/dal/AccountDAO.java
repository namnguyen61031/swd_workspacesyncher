/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import utils.Close;

/**
 *
 * @author Atheros
 */
public class AccountDAO {
    
    private final DBContext db;
    private final Close close;
    
    public AccountDAO() {
        this.db = new DBContext();
        this.close = new Close();
    }
    
    public boolean addAccount(Account account) {
        String sqlCommand = "INSERT INTO [Account] VALUES(?, ?)";
        
        Connection conn = null;
        PreparedStatement ps = null;
        boolean isSucess = true;
        
        try {
            conn = db.getConnection();
            ps = conn.prepareStatement(sqlCommand);
            ps.setString(1, account.getGmail());
            ps.setBoolean(2, account.isAutoSynched());
            ps.executeUpdate();
        } catch (Exception ex) {
            isSucess = false;
            Logger.getLogger(FileInformationDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            close.closePreparedStatement(ps);
            close.closeConnection(conn);
        }
        
        return isSucess;
    }
    
    public Account getAccount(String inputGmail) {
        String sqlCommand = "SELECT * FROM [Account] WHERE gmail = ?";
        
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean isSucess = true;
        Account account = null;
        
        try {
            conn = db.getConnection();
            ps = conn.prepareStatement(sqlCommand);
            ps.setString(1, inputGmail);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                int id = rs.getInt("id");
                String gmail = rs.getString("gmail");
                boolean isAutoSynched = rs.getBoolean("autoSynched");
                
                account = new Account(id, gmail, isAutoSynched);
            }
            
        } catch (Exception ex) {
            isSucess = false;
            Logger.getLogger(FileInformationDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            close.closeResultSet(rs);
            close.closePreparedStatement(ps);
            close.closeConnection(conn);
        }
        
        return account;
    }
    
    public boolean updateSynchronizeMode(Account account){
        String sqlCommand = "UPDATE [Account] SET autoSynched = ? WHERE id = ?";
        Connection conn = null;
        PreparedStatement ps = null;
        boolean isSucess = true;
        
        try {
            conn = db.getConnection();
            ps = conn.prepareStatement(sqlCommand);
            ps.setBoolean(1, account.isAutoSynched());
            ps.setInt(2, account.getId());
            ps.executeUpdate();
        } catch (Exception ex) {
            isSucess = false;
            Logger.getLogger(FileInformationDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            close.closePreparedStatement(ps);
            close.closeConnection(conn);
        }

        return isSucess;
    }
}
