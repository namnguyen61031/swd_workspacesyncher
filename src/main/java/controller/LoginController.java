package controller;


import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.util.store.FileDataStoreFactory;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.AccountManagementUtils;
import utils.GoogleDriveUtils;
import static utils.GoogleDriveUtils.CREDENTIALS_FOLDER;
import view.LoginView;

public class LoginController{
    private LoginView loginView;
    static LoginController INSTANCE;
    
    /* ====================== LoginController Initialization =================*/
    public LoginController() {
        INSTANCE = this;
        loginView = LoginView.getInstance();
    }
    public LoginView getLoginView() {
        return loginView;
    }

    public void setLoginView(LoginView loginView) {
        this.loginView = loginView;
    }
    
    /* ================== Action for LoginView's components ================= */
    public void startupLoginView(){
        try {
            boolean isLogged = AccountManagementUtils.isLoggedin();
            // If user has not signed-in any account
            if(!isLogged){
                this.displayLoginView();
                System.out.println("True");
            }else{
                // If already signed-in -> call TrackerController 
                TrackerController trackerController = new TrackerController();
                trackerController.displayTrackedFolders();
            }
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void displayLoginView(){            
        this.loginView.setVisible(true);
    }
    
    public void hideLoginView(){
        this.loginView.setVisible(false);
    }
    
    public boolean authenticate(){
        try {
            // Sign-in to one gmail user
            Credential credential = GoogleDriveUtils.getCredentials();
            
            // If StoredCredential file exists -> call TrackerController
            TrackerController trackerController = new TrackerController();
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        return true;
    }
    
    
    /* =============== Action for AccountPopup's components ================= */
    public boolean signOut(){
        boolean isSignedOut = false;
        try {
            boolean isLogged = AccountManagementUtils.isLoggedin();
            if(isLogged){
                // Delete StoredCredential file
                isSignedOut = AccountManagementUtils.deleteCopiedClientCredential();
                // Change the current email to Null
                GoogleDriveUtils.email = null;
                GoogleDriveUtils.DATA_STORE_FACTORY = new FileDataStoreFactory(CREDENTIALS_FOLDER);
            }   
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        return true;
    }
    
    
    public static LoginController getInstance(){
        if (INSTANCE == null){
            INSTANCE = new LoginController();
        }
        return INSTANCE;
    }
    
    
    public static void main(String[] args) {
        LoginController controller = new LoginController();
        controller.startupLoginView();
    }
}