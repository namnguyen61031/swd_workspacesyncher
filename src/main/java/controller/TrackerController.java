/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dal.AccountDAO;
import dal.FileInformationDAO;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.JFileChooser;
import javax.swing.tree.DefaultMutableTreeNode;
import model.Account;
import model.FileInformation;
import utils.GoogleDriveUtils;
import view.BrowsingDialog;
import view.MainView;

/**
 *
 * @author Nam Nguyen
 */
public class TrackerController {

    MainView mainView;
    public static Account account;
    FileInformationDAO fileInformationDAO;
    AccountDAO accountDAO;
    public WatchDir watchDir;

    public static boolean close = false;

    public TrackerController() {
        accountDAO = new AccountDAO();
        account = accountDAO.getAccount(GoogleDriveUtils.email);
        // System.out.println(account.getId());
        fileInformationDAO = new FileInformationDAO();
        mainView = new MainView();

        try {
            this.watchDir = new WatchDir(this, mainView);
            this.watchDir.execute();
        } catch (IOException ex) {
            Logger.getLogger(TrackerController.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            mainView.setTrackerCOntroller(this);
        } catch (Exception ex) {
            Logger.getLogger(TrackerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        mainView.setVisible(true);
        updateStatatusAfterOpen();
        displayTrackedFolders();
    }

    public boolean addNewTrackedFolder() {
        // Add new folder from local storage to be tracked by the system
        // Returns: 
        //    - status whether this operation is performed successfully

        // Browse local folder
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int i = fileChooser.showOpenDialog(mainView);

        File f = null;

        if (i == JFileChooser.APPROVE_OPTION) {
            f = fileChooser.getSelectedFile();
            if (f == null) {
                return false;
            }
            String filepath = f.getPath();
//            System.out.println("File path: " + filepath);
        } else if (i == JFileChooser.CANCEL_OPTION) {
            return false;
        }

        // Browse remote parent folder
        BrowsingDialog browsingDialog = new BrowsingDialog(null, true);
        browsingDialog.setVisible(true);

        String remoteParentId = browsingDialog.getRemoteId();

        // Add root folder to database
        FileInformation rootFolder = new FileInformation(f.getAbsolutePath());
        rootFolder.setDirectory(f.isDirectory());
        rootFolder.setAccountId(account.getId());
        rootFolder.setRemoteParentId(remoteParentId);
        rootFolder.setRootPath(f.getAbsolutePath());
        rootFolder.setStatus(FileInformation.STATUS_CHANGED);
        rootFolder.setLastModifiedTime(new Date(f.lastModified()));
        // insert root folder to database
        boolean statusCreated = fileInformationDAO.createFileInformation(rootFolder);
        try {
            // register root folder with watchService
            watchDir.registerAll(rootFolder.getAbsolutePath());
        } catch (IOException ex) {
            Logger.getLogger(TrackerController.class.getName()).log(Level.SEVERE, null, ex);
        }

        // add subitems to database
        List<File> filesInFolder;
        try {
            filesInFolder = Files.walk(Paths.get(rootFolder.getAbsolutePath()))
                    .map(Path::toFile)
                    .collect(Collectors.toList());

            // remove root from filesInFolder
            filesInFolder.remove(0);
            for (File subitem : filesInFolder) {
                FileInformation subFileInfo = new FileInformation(-1, subitem.getAbsolutePath(),
                        subitem.isDirectory(), null, null, rootFolder.getAbsolutePath(), FileInformation.STATUS_CHANGED,
                        new Date(subitem.lastModified()), account.getId());

                // insert subitem into databases
                fileInformationDAO.createFileInformation(subFileInfo);
            }
        } catch (IOException ex) {
            Logger.getLogger(TrackerController.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            // synch new added folder
            rootFolder = fileInformationDAO.getFileInformationByPath(rootFolder.getAbsolutePath(), account.getId());
            synchronize(rootFolder);
        } catch (IOException ex) {
            Logger.getLogger(TrackerController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return true;
    }

    public void displayTrackedFolders() {
        ArrayList<FileInformation> listFolder = fileInformationDAO.getAllTrackedFolders(account);
        for (FileInformation folder : listFolder) {
            try {
                watchDir.registerAll(folder.getAbsolutePath());
            } catch (IOException ex) {
                Logger.getLogger(TrackerController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        mainView.setTrackFolderList(listFolder);
    }

    public void setItemsToTreeNode(ArrayList<FileInformation> items, DefaultMutableTreeNode parentNode, HashMap<String, DefaultMutableTreeNode> pathNodeMapper) {
        for (FileInformation fileInfo : items) {
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(fileInfo);
            parentNode.add(node);
            System.out.println("File info: " + fileInfo);
            // add (path, node) to pathNodeMapper
            pathNodeMapper.put(fileInfo.getAbsolutePath(), node);
            if (fileInfo.isDirectory()) {
//                System.out.println(fileInfo);
                ArrayList<FileInformation> listSubfiles = fileInformationDAO.getAllSubFileInformationsLevelOne(fileInfo.getAbsolutePath());
                setItemsToTreeNode(listSubfiles, node, pathNodeMapper);
            }
        }
    }

    public void displaySubitems(FileInformation fileInformation) {
        // Display all subitem of parsed fileInformation
        // Arguments:
        //    - fileInformation: parent folder
        DefaultMutableTreeNode treeRoot = new DefaultMutableTreeNode(fileInformation);
        HashMap<String, DefaultMutableTreeNode> pathNodeMapper = new HashMap<>();
        // add (path, root) to pathNodeMapper
        pathNodeMapper.put(fileInformation.getAbsolutePath(), treeRoot);
        ArrayList<FileInformation> listSubfiles = fileInformationDAO.getAllSubFileInformations(fileInformation.getAbsolutePath());;

        for (FileInformation item : listSubfiles) {
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(item);
            DefaultMutableTreeNode parentNode = pathNodeMapper.get(item.getParent());
            pathNodeMapper.put(item.getAbsolutePath(), node);
            parentNode.add(node);
        }
//        setItemsToTreeNode(listSubfiles, treeRoot, pathNodeMapper);

        mainView.setRootToSubItemsTree(treeRoot);
        mainView.setPathNodeMapper(pathNodeMapper);
    }

    public boolean untrackFolder(FileInformation fileInformation) {
        // Untrack a folder from tracked list
        // Arguments:
        //   - fileId (int): id of folder to be untracked
        // Returns:
        //   - status whether this operation is performed successfully

        // delete records of this tracked folder in databases
        try {
            fileInformationDAO.deleteFolderAndSubitems(fileInformation.getAbsolutePath(), account.getId());
            watchDir.unregisterAll(fileInformation.getAbsolutePath());
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    public void setAutoSynch() {
        account.setAutoSynched(!account.isAutoSynched());
        accountDAO.updateSynchronizeMode(account);

        // call synch all
    }

    private int getParentStatus(FileInformation parent, String pathNotToConsider) {
        int undeliveredCount = 0;
        for (File childFile : parent.listFiles()) {
            if (!childFile.getAbsolutePath().equals(pathNotToConsider)) {
                undeliveredCount = fileInformationDAO.getAllUndeliveredSubitemsCount(new FileInformation(childFile.getAbsolutePath()));
                if (undeliveredCount > 0) {
                    return FileInformation.STATUS_CHANGED;
                }
            }
        }

        return FileInformation.STATUS_REMOVED;
    }

    private void synchParentsFirst(FileInformation fileInformation) {
        FileInformation parentFileInformation = fileInformation.getParentFile();
        if (parentFileInformation == null) {
            synchOneItem(fileInformation, true);
            return;
        }

        if (parentFileInformation.getRemoteFileId() == null) {
            synchParentsFirst(parentFileInformation);
            synchOneItem(parentFileInformation, true);
        } else {
            synchOneItem(fileInformation, true);
        }
    }

    private void synchDownToSubItems(FileInformation fileInformation) {
        // synch from this fileInformation to its subitems
        ArrayList<FileInformation> subFiles = fileInformationDAO.getAllSubFileInformations(fileInformation.getAbsolutePath());
        subFiles.add(0, fileInformation);  // add this fileInformation to be the first to be synched
        for (FileInformation fileInfo : subFiles) {
            boolean moveOn = synchOneItem(fileInfo, false);
            if (!moveOn) {
                break;
            }
        }
    }

    private boolean synchOneItem(FileInformation fileInformation, boolean keepStatus) {
        if (fileInformation.isDirectory()) {
            String remoteFileId = fileInformation.getRemoteFileId();
            if (fileInformation.getStatus() == FileInformation.STATUS_CHANGED) {
                // if synch a directory with status CHANGED -> 2 cases:
                //   1. New upload folder  ->  create new folder
                //   2. Files under it change  -> do nothing
                if (remoteFileId == null) {
                    try {
                        String remoteParentId = fileInformation.getRemoteParentId() != null
                                ? fileInformation.getRemoteParentId()
                                : fileInformation.getParentFile().getRemoteFileId();
                        remoteFileId = GoogleDriveUtils.createGoogleFolder(fileInformation.getName(), remoteParentId);
                        fileInformation.setRemoteFileId(remoteFileId);
                        fileInformation.setRemoteParentId(remoteParentId);
                    } catch (IOException ex) {
                        Logger.getLogger(TrackerController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if (!keepStatus) {
                    fileInformation.setStatus(FileInformation.STATUS_DELIVERED);
                }
                fileInformationDAO.updateAfterSynch(fileInformation);
            } else if (fileInformation.getStatus() == FileInformation.STATUS_REMOVED) {
                // if synch a directory with status REMOVED
                //  1. If folder has never been synch before -> do nothing
                //  2. If folder created before -> delete that folder
                if (remoteFileId != null) {
                    try {
                        GoogleDriveUtils.deleteFile(remoteFileId);
                    } catch (IOException ex) {
//                        Logger.getLogger(TrackerController.class.getName()).log(Level.SEVERE, null, ex);
                        return false;
                    }
                }

                if (!keepStatus) {
                    // no update status because it is removed from db
                    fileInformationDAO.deleteFileInformation(fileInformation.getId());
                }

            }
        } else {   // if item to synch is file
            String remoteFileId = fileInformation.getRemoteFileId();
            if (fileInformation.getStatus() == FileInformation.STATUS_CHANGED) {
                // Status changed, 2 cases:
                //  1. Uploaded before   ->  Delete then create
                //  2. Never uploaded before  ->  Upload only
                if (remoteFileId != null) {
                    try {
                        GoogleDriveUtils.deleteFile(remoteFileId);
                        String remoteParentId = fileInformation.getRemoteParentId() != null
                                ? fileInformation.getRemoteParentId()
                                : fileInformation.getParentFile().getRemoteFileId();
                        remoteFileId = GoogleDriveUtils.uploadFile(fileInformation.getAbsolutePath(), remoteParentId);
                        fileInformation.setRemoteFileId(remoteFileId);
                        fileInformation.setRemoteParentId(remoteParentId);
                    } catch (IOException ex) {
                        Logger.getLogger(TrackerController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    try {
                        String remoteParentId = fileInformation.getRemoteParentId() != null
                                ? fileInformation.getRemoteParentId()
                                : fileInformation.getParentFile().getRemoteFileId();
                        remoteFileId = GoogleDriveUtils.uploadFile(fileInformation.getAbsolutePath(), remoteParentId);
                        fileInformation.setRemoteFileId(remoteFileId);
                        fileInformation.setRemoteParentId(remoteParentId);
                    } catch (IOException ex) {
                        Logger.getLogger(TrackerController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                fileInformation.setStatus(FileInformation.STATUS_DELIVERED);
                fileInformationDAO.updateAfterSynch(fileInformation);
            } else if (fileInformation.getStatus() == FileInformation.STATUS_REMOVED) {

                // if this file is synch before -> delete the drive version of it
                if (remoteFileId != null) {
                    try {
                        GoogleDriveUtils.deleteFile(remoteFileId);
                    } catch (IOException ex) {
//                        Logger.getLogger(TrackerController.class.getName()).log(Level.SEVERE, null, ex);
                        return false;
                    }
                }

                fileInformationDAO.deleteFileInformation(fileInformation.getId());
            }
        }

        return true;
    }

    public void synchronize(FileInformation fileInformation) throws IOException {
        System.out.println("File id: " + fileInformation.getId());
        fileInformation = fileInformationDAO.getFileInformationByPath(fileInformation.getAbsolutePath(), account.getId());
        // First, synch up to parents and root
        synchParentsFirst(fileInformation);

        // Then synch down to its subitems
        synchDownToSubItems(fileInformation);

        FileInformation parent = fileInformation.getParentFile();
        while (parent != null) {
            // update status
            boolean isAllDelivered = fileInformationDAO.isAllDeliver(parent.getAbsolutePath(), account.getId());
            System.out.println(" Is delivered: " + isAllDelivered);
            if ((parent.getStatus() == FileInformation.STATUS_DELIVERED && isAllDelivered) || (parent.getStatus() != FileInformation.STATUS_DELIVERED && !isAllDelivered)) {
                return;
            }

            parent.setStatus(isAllDelivered ? FileInformation.STATUS_DELIVERED : parent.getStatus());
            fileInformationDAO.updateAfterSynch(parent);

            parent = parent.getParentFile();
        }

        if (mainView.getSelectedRootFolder() != null) {
            displaySubitems(mainView.getSelectedRootFolder());
        }

    }

    public void synchronizeAll() throws IOException {
        ArrayList<FileInformation> listFolder = fileInformationDAO.getAllTrackedFolders(account);
        for (FileInformation folder : listFolder) {
            synchronize(folder);
        }
    }

    /*
    *    
    Track status section
    *
     */
    public void checkForAddOrChangeThenUpdate(FileInformation rootFolder, boolean notify) {
        Path rootPath = Paths.get(rootFolder.getAbsolutePath());
        try {
            Files.walkFileTree(rootPath, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path t, BasicFileAttributes bfa) throws IOException {
                    //notify for create here
                    if (notify) {
                        mainView.notifyUpdateStatus(new FileInformation(t.toString()), StandardWatchEventKinds.ENTRY_CREATE);
                    }
                    checkAndUpdate(t.toString(), new Date(bfa.lastModifiedTime().toMillis()), account.getId());
                    return super.visitFile(t, bfa);
                }

                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes bfa)
                        throws IOException {
                    //notify for create here
                    if (notify) {
                        mainView.notifyUpdateStatus(new FileInformation(dir.toString()), StandardWatchEventKinds.ENTRY_CREATE);
                    }
                    checkAndUpdate(dir.toString(), new Date(bfa.lastModifiedTime().toMillis()), account.getId());
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException ex) {
            Logger.getLogger(TrackerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void checkAndUpdate(String path, Date lastModified, int accountID) {
        FileInformation fileInformation = fileInformationDAO.getFileInformationByPath(path, accountID);
        //new file created
        if (fileInformation == null) {
            fileInformation = new FileInformation(path, FileInformation.STATUS_CHANGED, accountID);
            FileInformation parent = fileInformation.getParentFile();
            if (parent != null) {
                fileInformation.setRemoteParentId(parent.getRemoteFileId());
            }
            fileInformationDAO.createFileInformation(fileInformation);

            updateParentStatusToChange(fileInformation);

        } else {
            //file changed content
            Date dbLastModified = fileInformation.getLastModifiedTime();
            if ((long)(lastModified.getTime() / 1000) > (long)(dbLastModified.getTime() / 1000)) {
                if (fileInformation.getStatus() != FileInformation.STATUS_CHANGED) {
                    fileInformationDAO.updateFileInformationStatusAndLastModificationTime(
                            fileInformation.getId(),
                            FileInformation.STATUS_CHANGED, lastModified);
                    //If a file changed, then the update all parent status to change
                    updateParentStatusToChange(fileInformation);

                }
            }
        }
    }

    //a changed file is a new file created or changed content
    public void updateParentStatusToChange(FileInformation changedFile) {
        //If a file changed, then the update all parent status to change
        FileInformation parent = changedFile.getParentFile();
        while (parent != null && !parent.getAbsolutePath().equals(parent.getRootPath())) {
            if (parent.getStatus() == FileInformation.STATUS_DELIVERED) {
                fileInformationDAO.updateFileInformationStatus(parent.getId(),
                        FileInformation.STATUS_CHANGED);
            }
            parent = parent.getParentFile();
        }
        if (parent != null) {
            //parent is root
            if (parent.getStatus() == FileInformation.STATUS_DELIVERED) {
                fileInformationDAO.updateFileInformationStatus(parent.getId(),
                        FileInformation.STATUS_CHANGED);
            }
        }
    }

    public boolean updateFileStatusForDeletion() {
        ArrayList<FileInformation> trackedFolder = fileInformationDAO.getAllTrackedFolders(account);
        trackedFolder.forEach((fileInformation) -> {
            checkForDeletionThenUpdate(fileInformation);
        });
        return true;
    }

    private void checkForDeletionThenUpdate__(FileInformation fileInformation) {
        ArrayList<FileInformation> subItems = fileInformationDAO.getAllSubFileInformations(fileInformation.getRemoteFileId());
        subItems.stream().map((file) -> {
            if (!file.exists()
                    && file.getStatus() != FileInformation.STATUS_REMOVED) {
                fileInformationDAO.updateFileInformationStatus(file.getId(),
                        FileInformation.STATUS_REMOVED);
            }
            return file;
        }).filter((file) -> (file.isDirectory())).forEachOrdered((file) -> {
            checkForDeletionThenUpdate(file);
        });

    }

    private void checkForDeletionThenUpdate(FileInformation fileInformation) {
        ArrayList<FileInformation> subItems = fileInformationDAO.getAllSubFileInformations(fileInformation.getRemoteFileId());
        subItems.forEach((file) -> {
            if (!file.exists() && file.getStatus() != FileInformation.STATUS_REMOVED) {
                fileInformationDAO.updateDeleletionStatus(file.getAbsolutePath(), account.getId());
            } else if (file.exists() && file.isDirectory()) {
                checkForAddOrChangeThenUpdate(file, false);
            }
        });

    }

    public void updateStatatusAfterOpen() {
        ArrayList<FileInformation> trackedFolders = fileInformationDAO.getAllTrackedFolders(account);

        //update for created or changed content file
        trackedFolders.forEach((fileInformation) -> {
            checkForAddOrChangeThenUpdate(fileInformation, false);
        });

        //Update deteted file
        trackedFolders.forEach((fileInformation) -> {
            checkForDeletionThenUpdate(fileInformation);
        });
    }

    public boolean isAllChildrenDelivered(FileInformation fileInformation) {
        if (fileInformationDAO.getAllUndeliveredSubitemsCount(fileInformation) == 0) {
            return true;
        }

        return false;
    }

    public static void main(String[] args) {
        TrackerController controller = new TrackerController();
        controller.displayTrackedFolders();
    }
}
