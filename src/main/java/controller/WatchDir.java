/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dal.FileInformationDAO;
import java.nio.file.*;
import static java.nio.file.StandardWatchEventKinds.*;
import static java.nio.file.LinkOption.*;
import java.nio.file.attribute.*;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingWorker;
import model.FileInformation;
import view.MainView;

/**
 *
 * @author Atheros
 */
public class WatchDir extends SwingWorker<Void, NotifyObject> {

    private final WatchService watcher;
    private final Map<WatchKey, Path> keys;
    private final FileInformationDAO fileDAO;
    MainView mainView;
    TrackerController trackerController;
    public static boolean stop = false;

    static <T> WatchEvent<T> cast(WatchEvent<?> event) {
        return (WatchEvent<T>) event;
    }

    private void register(Path dir) throws IOException {
        WatchKey key = dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
        keys.put(key, dir);
    }

    public void registerAll(final String rootFolder) throws IOException {
        final Path start = Paths.get(rootFolder);
        // register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                    throws IOException {
                register(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    private void unregister(Path dir) {
        keys.entrySet().removeIf(entry -> (dir.equals(entry.getValue())));
    }

    public void unregisterAll(final String rootFolder) throws IOException {
        final Path start = Paths.get(rootFolder);
        // register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                    throws IOException {
                unregister(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    WatchDir(TrackerController trackerController, MainView mainView) throws IOException {
        this.watcher = FileSystems.getDefault().newWatchService();
        this.keys = new HashMap<>();
        this.fileDAO = new FileInformationDAO();

        this.trackerController = trackerController;
        this.mainView = mainView;
    }

    @Override
    protected Void doInBackground() throws Exception {
        while (!stop) {

            // wait for key to be signalled
            WatchKey key;
            try {
                key = watcher.take();
            } catch (InterruptedException x) {
                return null;
            }

            Path dir = keys.get(key);
            System.out.println(dir);
            if (dir == null) {
                System.err.println("WatchKey not recognized!!");
                continue;
            }

            key.pollEvents().forEach((event) -> {
                WatchEvent.Kind kind = event.kind();
                // TBD - provide example of how OVERFLOW event is handled
                if (!(kind == OVERFLOW)) {
                    // Context for directory entry event is the file name of entry
                    WatchEvent<Path> ev = cast(event);
                    Path name = ev.context();
                    Path child = dir.resolve(name);
                    // print out event
                    System.out.format("%s: %s\n", event.kind().name(), child);
                    // if directory is created, and watching recursively, then
                    // register it and its sub-directories
                    FileInformation fileInformation = new FileInformation(
                            child.toString(),
                            FileInformation.STATUS_CHANGED,
                            TrackerController.account.getId()
                    );

                    FileInformation dbFile = fileDAO.getFileInformationByPath(
                            child.toString(),
                            TrackerController.account.getId()
                    );

                    if (kind == ENTRY_CREATE) {
                        try {
                            if (Files.isDirectory(child, NOFOLLOW_LINKS)) {
                                registerAll(child.toString());
                            }
                        } catch (IOException x) {
                            // ignore to keep sample readbale
                        }

                        if (dbFile == null) {
                            //this file path does not exist on database
                            System.out.println("Create new file");
                            fileDAO.createFileInformation(fileInformation);
                        } else {
                            //this file path exist on database
                            //maybe file has been deleted but not synchronized
                            System.out.println("Update file ID " + dbFile.getId());
                            fileDAO.updateFileInformationStatusAndLastModificationTime(
                                    dbFile.getId(),
                                    FileInformation.STATUS_CHANGED,
                                    fileInformation.getLastModifiedTime()
                            );
                        }

                        // call notifyUpdateStatus from main view
                        publish(new NotifyObject(fileInformation, kind));

                    } else if (kind == ENTRY_MODIFY) {
                        System.out.println("Update file ID " + dbFile.getId());
                        if (dbFile.exists()) {
                            fileDAO.updateFileInformationStatusAndLastModificationTime(
                                    dbFile.getId(),
                                    FileInformation.STATUS_CHANGED,
                                    fileInformation.getLastModifiedTime()
                            );
                        }
                        if (fileInformation.isDirectory()) {
                            trackerController.updateParentStatusToChange(dbFile);
                        }

                    } else if (kind == ENTRY_DELETE) {

                        // call notifyUpdateStatus from main view
                        mainView.notifyUpdateStatus(dbFile, ENTRY_DELETE);

                        System.out.println("Delete file ID " + dbFile.getId());
                        fileDAO.updateDeleletionStatus(
                                dbFile.getAbsolutePath(),
                                TrackerController.account.getId()
                        );

                    }

                    //auto sync mode
                    if (TrackerController.account.isAutoSynched()) {
                        try {
                            trackerController.synchronize(dbFile != null ? dbFile : fileInformation);
                        } catch (Exception ex) {
                            Logger.getLogger(WatchDir.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });

            // reset key and remove from set if directory no longer accessible
            boolean valid = key.reset();
            if (!valid) {
                keys.remove(key);

//                // all directories are inaccessible
//                if (keys.isEmpty()) {
//                    break;
//                }
            }
        }
        return null;
    }

    @Override
    protected void process(List<NotifyObject> list) {
        NotifyObject notifyObject = list.get(0);

        if (notifyObject.file.isDirectory()) {
            trackerController.checkForAddOrChangeThenUpdate(notifyObject.file, true);
        } else {
            mainView.notifyUpdateStatus(notifyObject.file, notifyObject.kind);
        }

    }

}

class NotifyObject {

    FileInformation file;
    WatchEvent.Kind kind;

    public NotifyObject(FileInformation file, WatchEvent.Kind kind) {
        this.file = file;
        this.kind = kind;
    }

}
