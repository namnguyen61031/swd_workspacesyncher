/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import controller.LoginController;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.GoogleDriveUtils;

/**
 *
 * @author Nam Nguyen
 */
public class TestGoogleDriveUtils {

    public static void main(String[] args) {

        testGetDriveService();
    }

    public static void testGetDriveService() {
        try {
            // Get the Drive Service to access to call Drive apis
            Drive service = GoogleDriveUtils.getDriveService();

//            // Print the names and IDs for up to 10 files.
//            FileList result = service.files().list().setPageSize(10).setFields("nextPageToken, files(id, name)").execute();
//            List<File> files = result.getFiles();
//            if (files == null || files.isEmpty()) {
//                System.out.println("No files found.");
//            } else {
//                System.out.println("Files:");
//                for (File file : files) {
//                    System.out.printf("%s (%s)\n", file.getName(), file.getId());
//                }
//            }
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void testCreateFolder() {
        try {
            GoogleDriveUtils.createGoogleFolder("TEST_107", "1h2BwY_IYLk8KF9VMHGK1Hy7sO7Ow9OCZ");
        } catch (IOException ex) {
            Logger.getLogger(TestGoogleDriveUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void testCreateFile() {
        try {
            GoogleDriveUtils.uploadFile("C:/Users/Nam Nguyen/Desktop/TeamCockies_KaggleIMet.pptx", "1h2BwY_IYLk8KF9VMHGK1Hy7sO7Ow9OCZ");
        } catch (IOException ex) {
            Logger.getLogger(TestGoogleDriveUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void testDeleteFile() {
        try {
            GoogleDriveUtils.deleteFile("1BQx2QfF1lKXkcNx_Z3Y8f7yhQPlptPez");
        } catch (IOException ex) {
            Logger.getLogger(TestGoogleDriveUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void testGetGoogleSubFolders() {
        try {
            List<File> list = GoogleDriveUtils.getGoogleSubFolders("1y332IJjH1Wpu60SFmr2u9g156S88gFJR");
            for (File file : list) {
                System.out.println(file.getName());
            }
        } catch (Exception ex) {
            Logger.getLogger(TestGoogleDriveUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void testGetGoogleRootFolders() {
        try {
            List<File> list = GoogleDriveUtils.getGoogleRootFolders();
            for (File file : list) {
                System.out.printf("%s %s \n", file.getName(), file.getId());
            }
        } catch (Exception ex) {
            Logger.getLogger(TestGoogleDriveUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
