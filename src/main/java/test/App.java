package test;

import controller.LoginController;
import view.LoginView;

public class App {
    public static void main(String[] args) {
        // Assemble all the pieces of the MVC
        LoginView loginView = new LoginView("First Login");
        LoginController loginController = new LoginController();
        loginController.setLoginView(loginView);
        loginController.startupLoginView();
    }
}