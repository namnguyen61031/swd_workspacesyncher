/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.LoginController;
import controller.TrackerController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import java.nio.file.WatchEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import model.FileInformation;

/**
 *
 * @author tqtie
 */
public class MainView extends javax.swing.JFrame {

    private TrackerController trackerController;
    
    public static MainView INSTANCE;

    DefaultListModel<FileInformation> defaultListModel;
    DefaultTreeModel defaultTreeModel;
    HashMap<String, DefaultMutableTreeNode> pathNodeMapper;

    
    
    public void setBorder() {
        Border border = trackedFolderLbl.getBorder();
        Border margin = new EmptyBorder(10, 10, 10, 10);
        trackedFolderLbl.setBorder(new CompoundBorder(border, margin));
        filesInFolderLbl.setBorder(new CompoundBorder(border, margin));
    }

    /**
     * Creates new form MainView
     */
    public MainView() {
        this.setTitle("Main View");
        initComponents();
        defaultTreeModel = new DefaultTreeModel(null);
        folderTree.setModel(defaultTreeModel);
        setBorder();
        INSTANCE = this;
        setLocationRelativeTo(null);
    }


    
    
    public void setTrackerCOntroller(TrackerController trackerController) throws Exception {
        // Set tracker controller to trackerController attribute of the class,
        // if is currently null
        // Arguments:
        //    - trackerController

        if (this.trackerController != null) {
            throw new Exception("trackerCOntroller of MainView object has already exists");
        }

        this.trackerController = trackerController;
        
        //display user email
        emailLbl.setText(TrackerController.account.getGmail());
    }

    public FileInformation getSelectedRootFolder(){
        return folderList.getSelectedValue();
    }

    public void setTrackFolderList(ArrayList<FileInformation> listFolder) {
        // Add list of tracked folder to Jlist
        // Arguments:
        //    - listFolder: array list of tracked folder parsed by controller
        defaultListModel = new DefaultListModel<>();
        for (FileInformation fileInfo : listFolder) {
            defaultListModel.addElement(fileInfo);
        }

        folderList.setModel(defaultListModel);
        folderList.setSelectedIndex(0);
        folderListMouseClicked(null);
    }

    public void setRootToSubItemsTree(DefaultMutableTreeNode root) {
        System.out.println(root);
        defaultTreeModel.setRoot(root);
    }
    
    public void setPathNodeMapper(HashMap<String, DefaultMutableTreeNode> pathNodeMapper) {
        this.pathNodeMapper = pathNodeMapper;
    }
    
    public void setStatusTreeItemsRecursively(DefaultMutableTreeNode child, int newStatus){
        FileInformation fileInfo =  (FileInformation) child.getUserObject();
        fileInfo.setStatus(newStatus);
        int childCount = child.getChildCount();
        for (int i=0;i<childCount;i++){
            setStatusTreeItemsRecursively((DefaultMutableTreeNode) child.getChildAt(i), newStatus);
        }
    }

    public void notifyUpdateStatus(FileInformation fileInformation, WatchEvent.Kind kind) {
        // Called by watch service when a folder/ file is deleted or modified
        // when a folder is deleted: this folder status -> removed and so do its subitems
        // when a file is deleted this file status -> removed
        // when a file is mofified: this file status -> changed 
        // Always update status to change back to root node
        
        if (kind == ENTRY_DELETE) {
            // remove recursive all element of tree
            DefaultMutableTreeNode deletedNode = pathNodeMapper.get(fileInformation.getAbsolutePath());
            setStatusTreeItemsRecursively(deletedNode, FileInformation.STATUS_REMOVED);
            
        }else if(kind == ENTRY_CREATE){
            // Add new node to tree containing this information
            DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(fileInformation);
            String parentAbsolutePath = fileInformation.getParent();
            fileInformation.setStatus(FileInformation.STATUS_CHANGED);
            DefaultMutableTreeNode parentNode = pathNodeMapper.get(parentAbsolutePath);
            FileInformation parentInformation = (FileInformation) parentNode.getUserObject();

            parentNode.add(newNode);
            // add new node to pathNodeMapper
            pathNodeMapper.put(fileInformation.getAbsolutePath(), newNode);
//            defaultTreeModel.reload();
        }
        
        // set change status to root
        DefaultMutableTreeNode node = pathNodeMapper.get(fileInformation.getAbsolutePath());
        updateStatusToRoot(node, FileInformation.STATUS_CHANGED);
        
        defaultTreeModel.reload();
    }
    
    private void updateStatusToRoot(DefaultMutableTreeNode node, int newStatus){
        
        while(true){
           DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) node.getParent();
           if(parentNode == null){
               break;
           }else{
               FileInformation parentInformation = (FileInformation) parentNode.getUserObject();
               if(newStatus == FileInformation.STATUS_DELIVERED){
                   if(trackerController.isAllChildrenDelivered(parentInformation)){
                       parentInformation.setStatus(newStatus);
                   }
               }else{
                    parentInformation.setStatus(newStatus);
               }
              
               node = parentNode;
           }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        folderTreePn = new javax.swing.JPanel();
        folderTreeScrPn = new javax.swing.JScrollPane();
        folderTree = new javax.swing.JTree();
        trackedFolderLbl = new javax.swing.JLabel();
        filesInFolderLbl = new javax.swing.JLabel();
        folderListPn = new javax.swing.JPanel();
        folderListScrPn = new javax.swing.JScrollPane();
        folderList = new javax.swing.JList<>();
        addNewTrackedFolderBt = new javax.swing.JButton();
        topBarPn = new javax.swing.JPanel();
        syncModeLbl = new javax.swing.JLabel();
        autoSyncToggleBt = new javax.swing.JToggleButton();
        emailLbl = new javax.swing.JLabel();
        signout = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        folderTree.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                folderTreeMouseClicked(evt);
            }
        });
        folderTreeScrPn.setViewportView(folderTree);

        javax.swing.GroupLayout folderTreePnLayout = new javax.swing.GroupLayout(folderTreePn);
        folderTreePn.setLayout(folderTreePnLayout);
        folderTreePnLayout.setHorizontalGroup(
            folderTreePnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(folderTreePnLayout.createSequentialGroup()
                .addComponent(folderTreeScrPn, javax.swing.GroupLayout.PREFERRED_SIZE, 433, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 6, Short.MAX_VALUE))
        );
        folderTreePnLayout.setVerticalGroup(
            folderTreePnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(folderTreeScrPn, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
        );

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("view/Bundle"); // NOI18N
        trackedFolderLbl.setText(bundle.getString("MainView.trackedFolderLbl.text")); // NOI18N
        trackedFolderLbl.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        filesInFolderLbl.setText(bundle.getString("MainView.filesInFolderLbl.text")); // NOI18N
        filesInFolderLbl.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        folderList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                folderListMouseClicked(evt);
            }
        });
        folderListScrPn.setViewportView(folderList);

        javax.swing.GroupLayout folderListPnLayout = new javax.swing.GroupLayout(folderListPn);
        folderListPn.setLayout(folderListPnLayout);
        folderListPnLayout.setHorizontalGroup(
            folderListPnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(folderListPnLayout.createSequentialGroup()
                .addComponent(folderListScrPn, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        folderListPnLayout.setVerticalGroup(
            folderListPnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(folderListScrPn, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
        );

        addNewTrackedFolderBt.setText(bundle.getString("MainView.addNewTrackedFolderBt.text")); // NOI18N
        addNewTrackedFolderBt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addNewTrackedFolderBtActionPerformed(evt);
            }
        });

        topBarPn.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        syncModeLbl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        syncModeLbl.setText(bundle.getString("MainView.syncModeLbl.text")); // NOI18N

        autoSyncToggleBt.setText(bundle.getString("MainView.autoSyncToggleBt.text")); // NOI18N
        autoSyncToggleBt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                autoSyncToggleBtActionPerformed(evt);
            }
        });

        emailLbl.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        emailLbl.setText(bundle.getString("MainView.emailLbl.text")); // NOI18N
        emailLbl.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));

        signout.setForeground(new java.awt.Color(0, 0, 204));
        signout.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        signout.setText(bundle.getString("MainView.signout.text")); // NOI18N
        signout.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        signout.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        signout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                signoutMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout topBarPnLayout = new javax.swing.GroupLayout(topBarPn);
        topBarPn.setLayout(topBarPnLayout);
        topBarPnLayout.setHorizontalGroup(
            topBarPnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(topBarPnLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(syncModeLbl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(autoSyncToggleBt)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(emailLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(signout, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32))
        );
        topBarPnLayout.setVerticalGroup(
            topBarPnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(topBarPnLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(topBarPnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(autoSyncToggleBt)
                    .addComponent(syncModeLbl)
                    .addComponent(emailLbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(signout, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        jButton1.setText(bundle.getString("MainView.jButton1.text")); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(topBarPn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(addNewTrackedFolderBt)
                        .addGap(57, 57, 57)
                        .addComponent(jButton1))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(trackedFolderLbl)
                        .addComponent(folderListPn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(80, 80, 80)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(folderTreePn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(32, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(filesInFolderLbl)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(topBarPn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(54, 54, 54)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(trackedFolderLbl)
                    .addComponent(filesInFolderLbl))
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(folderTreePn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(folderListPn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addNewTrackedFolderBt)
                    .addComponent(jButton1))
                .addGap(31, 31, 31))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addNewTrackedFolderBtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addNewTrackedFolderBtActionPerformed
        trackerController.addNewTrackedFolder();
        trackerController.displayTrackedFolders();
    }//GEN-LAST:event_addNewTrackedFolderBtActionPerformed

    private void folderListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_folderListMouseClicked
        // Handler event when click on an item of folderList
        // Dipslay subitems on jtree on the right side of the screen
        
        if (evt == null) {
            FileInformation fileInfo = folderList.getSelectedValue();
            if (fileInfo == null) {
                return;
            }
            // display subitem of fileInfo
            trackerController.displaySubitems(fileInfo);
            return;
        }

        if (SwingUtilities.isRightMouseButton(evt)) {
            int row = folderList.locationToIndex(evt.getPoint());
            folderList.setSelectedIndex(row);

            PopUpJList menu = new PopUpJList();
            menu.show(evt.getComponent(), evt.getX(), evt.getY());
        } else {
            FileInformation fileInfo = folderList.getSelectedValue();
            if (fileInfo == null) {
                return;
            }
            // display subitem of fileInfo
            trackerController.displaySubitems(fileInfo);
        }
    }//GEN-LAST:event_folderListMouseClicked

    private void folderTreeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_folderTreeMouseClicked
        // Check if right click
        // Show option menu + Hightlight selected item
        if (SwingUtilities.isRightMouseButton(evt)) {
            int selRow = folderTree.getRowForLocation(evt.getX(), evt.getY());
            TreePath selPath = folderTree.getPathForLocation(evt.getX(), evt.getY());
            folderTree.setSelectionPath(selPath);
            if (selRow > -1) {
                folderTree.setSelectionRow(selRow);
            }
            PopUpJTree menu = new PopUpJTree();
            menu.show(evt.getComponent(), evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_folderTreeMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            trackerController.synchronizeAll();
        } catch (IOException ex) {
            Logger.getLogger(MainView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void autoSyncToggleBtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_autoSyncToggleBtActionPerformed
        if (autoSyncToggleBt.getText().equals("ON")) {
            autoSyncToggleBt.setText("OFF");
        } else if (autoSyncToggleBt.getText().equals("OFF")) {
            autoSyncToggleBt.setText("ON");
        } else {
            return;
        }
        trackerController.setAutoSynch();
    }//GEN-LAST:event_autoSyncToggleBtActionPerformed

    private void signoutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_signoutMouseClicked
        boolean success = LoginController.getInstance().signOut();
        if(success){
            MainView.INSTANCE.dispose();
            this.setVisible(false);
            LoginController.getInstance().displayLoginView();
            
        }else{
            String msg  = "You can not sign out now";
            JOptionPane.showMessageDialog(null, msg);
        }
    }//GEN-LAST:event_signoutMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainView().setVisible(true);
            }
        });
    }

    class PopUpJList extends JPopupMenu {

        JMenuItem itemUntrack;

        public PopUpJList() {
            itemUntrack = new JMenuItem("Untrack");
            add(itemUntrack);
            itemUntrack.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    int selectedIndex = folderList.getSelectedIndex();
                    trackerController.untrackFolder(folderList.getSelectedValue());
                    defaultListModel.remove(selectedIndex);
                    folderList.setSelectedIndex(0);
                    folderListMouseClicked(null);
                    if (folderList.isSelectionEmpty()){
                        setRootToSubItemsTree(null);
                    }
                }
            });
        }

    }

    class PopUpJTree extends JPopupMenu {

        JMenuItem itemUntrack;

        public PopUpJTree() {
            itemUntrack = new JMenuItem("Synch now");
            add(itemUntrack);
            itemUntrack.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    TreePath[] paths = folderTree.getSelectionPaths();
                    DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) folderTree.getLastSelectedPathComponent();
                    FileInformation fileInformation = (FileInformation) selectedNode.getUserObject();

                    try {

                        if (fileInformation.getStatus() == FileInformation.STATUS_CHANGED) {
                            System.out.println("Status changed");
                            trackerController.synchronize(fileInformation);
                            setStatusTreeItemsRecursively(selectedNode, FileInformation.STATUS_DELIVERED);
                            updateStatusToRoot(selectedNode, FileInformation.STATUS_DELIVERED);
//                            defaultTreeModel.reload();
                        } else if (fileInformation.getStatus() == FileInformation.STATUS_REMOVED) {
                            System.out.println("Status removed");
                            // remove this mode from tree
                            defaultTreeModel.removeNodeFromParent(selectedNode);

                            // remove recursive all element of tree
                            if (paths != null) {
                                for (TreePath path : paths) {
                                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
                                    if (node.getParent() != null) {
                                        defaultTreeModel.removeNodeFromParent(node);
                                    }
                                }
                            }
                            trackerController.synchronize(fileInformation);
                            updateStatusToRoot(selectedNode, FileInformation.STATUS_DELIVERED);
                        }

                    } catch (IOException ex) {
                        Logger.getLogger(MainView.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            });
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addNewTrackedFolderBt;
    private javax.swing.JToggleButton autoSyncToggleBt;
    private javax.swing.JLabel emailLbl;
    private javax.swing.JLabel filesInFolderLbl;
    private javax.swing.JList<FileInformation> folderList;
    private javax.swing.JPanel folderListPn;
    private javax.swing.JScrollPane folderListScrPn;
    private javax.swing.JTree folderTree;
    private javax.swing.JPanel folderTreePn;
    private javax.swing.JScrollPane folderTreeScrPn;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel signout;
    private javax.swing.JLabel syncModeLbl;
    private javax.swing.JPanel topBarPn;
    private javax.swing.JLabel trackedFolderLbl;
    // End of variables declaration//GEN-END:variables
}
