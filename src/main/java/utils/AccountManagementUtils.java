package utils;

import java.io.IOException;

/**
 *
 * @author NamAnh
 */
public class AccountManagementUtils {
    // Directory to store user credentials for this application.
    private static final java.io.File CREDENTIALS_FOLDER //
            = new java.io.File("src/main/resources");
    
    /* Using this function to the copiedClientCredentialFile */
    public static boolean deleteCopiedClientCredential() throws IOException{
        boolean isDeleted = false;
        
        java.io.File copiedClientSecretFilePath = new java.io.File(CREDENTIALS_FOLDER, "StoredCredential");
 
        isDeleted = copiedClientSecretFilePath.delete(); 
        if(isDeleted)
            System.out.println("Deletion successful."); 
        
        return isDeleted;
    }
    
    /* Using this function to check if user is already logged-in or not*/
    public static boolean isLoggedin() throws IOException{
        boolean isCredentialExisted = false;
        
        java.io.File copiedClientSecretFilePath = new java.io.File(CREDENTIALS_FOLDER, "StoredCredential");
 
        if (copiedClientSecretFilePath.exists()) {
            isCredentialExisted = true;
        }
        
        return isCredentialExisted;
    }
}
