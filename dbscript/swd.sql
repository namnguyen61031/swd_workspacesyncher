CREATE DATABASE [SWD_workspace_synch]
GO
USE [SWD_workspace_synch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[gmail] [nvarchar](100) NOT NULL,
	[autoSynched] [bit] NOT NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileInformation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[absolutePath] [nvarchar](200) NOT NULL,
	[isDirectory] [bit] NOT NULL,
	[remoteParentId] [nvarchar](200) NULL,
	[remoteFileId] [nvarchar](200) NULL,
	[rootPath] [nvarchar](200) NULL,
	[status] [int] NOT NULL,
	[lastModifiedTime] [datetime] NOT NULL,
	[accountId] [int] NOT NULL,
 CONSTRAINT [PK_FileInformation] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[FileInformation]  WITH CHECK ADD  CONSTRAINT [FK_FileInformation_Account] FOREIGN KEY([accountId])
REFERENCES [dbo].[Account] ([id])
GO
ALTER TABLE [dbo].[FileInformation] CHECK CONSTRAINT [FK_FileInformation_Account]
GO
